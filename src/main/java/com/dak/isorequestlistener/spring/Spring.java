/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.isorequestlistener.spring;

import com.dak.isorequestlistener.dao.LogActivityDao;
import javax.naming.ConfigurationException;
import org.jpos.core.Configuration;
import org.jpos.q2.QBeanSupport;
import org.jpos.util.Log;
import org.jpos.util.NameRegistrar;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

/**
 *
 * @author Andri D Septian
 */
public class Spring extends QBeanSupport {

    Log log = Log.getLog("Q2", getClass().getName());

    private static LogActivityDao logActivityDao;

    public static LogActivityDao getLogActivityDao() {
        return logActivityDao;
    }

    public static void setLogActivityDao(LogActivityDao logActivityDao) {
        Spring.logActivityDao = logActivityDao;
    }
    
    Configuration conf;

    @Override
    public void initService() throws ConfigurationException {
//        ApplicationContext context = new FileSystemXmlApplicationContext("src/main/resources/ApplicationContext.xml");
        ApplicationContext context = new FileSystemXmlApplicationContext("ApplicationContext.xml");
        setLogActivityDao(context.getBean("LogActivityDao", LogActivityDao.class));
        
        log.info("Init DB has Successfull");
        log.info("Service Started");
    }
    
    @Override
    protected void destroyService() throws Exception {
        log.info("Spring " + getName() + " is stopped");
        NameRegistrar.unregister("SPRING");
    }

    @Override
    public void setConfiguration(Configuration cfg) throws org.jpos.core.ConfigurationException {
        this.conf = cfg;
    }

}
