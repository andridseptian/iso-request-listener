/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.isorequestlistener.core.iso;

import java.io.IOException;
import java.util.Arrays;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISORequestListener;
import org.jpos.iso.ISOSource;
import org.jpos.space.Space;
import org.jpos.space.SpaceFactory;
import org.jpos.transaction.Context;
import org.jpos.util.Log;

/**
 *
 * @author Andri D Septian
 */
public class ISOGatewayRequestListener implements ISORequestListener {

    Log log = Log.getLog("Q2", getClass().getName());
    Configuration cfg;

    @Override
    public boolean process(ISOSource isos, ISOMsg isomsg) {
        try {
            if ("0800".equals(isomsg.getMTI())) {
                isomsg.setResponseMTI();
                isomsg.set(39, "00");
                isos.send(isomsg);
            } else if ("0810".equals(isomsg.getMTI())) {
                log.info("DROP 0810 Request");
            } else if (isomsg.isResponse()) {
                log.info("DROP Late Response");
            } else {
                Context ctx = new Context();
                Space sp = SpaceFactory.getSpace();

                String txn = cfg.get("processor", "bypass-pc");
                int destinationNumber = cfg.getInt("de_destination_num", 2);
                int proCode = cfg.getInt("processing_code", 3);
                int msgContentDe = cfg.getInt("de_private_msg_content", 48);
                String delimiter = cfg.get("delimiter", "\\|");
                int[] mandatoryField = new int[]{destinationNumber, proCode, 7, 11, 32, 37, 41, msgContentDe};
                if (isomsg.hasFields(mandatoryField)) {
                    String[] request = isomsg.getString(msgContentDe).split(delimiter);
                    sp.out(txn, ctx, 60000);
                } else {
                    log.error("Missing Mandatory Parameter, must contain all these field : " + Arrays.toString(mandatoryField));
                    if (isomsg.isRequest()) {
                        isomsg.setResponseMTI();
                    }

                    isomsg.set(39, "99");
                    isos.send(isomsg);
                }
            }
        } catch (ISOException | IOException isoe) {
            log.error(isoe);
        }

        return true;
    }

}
