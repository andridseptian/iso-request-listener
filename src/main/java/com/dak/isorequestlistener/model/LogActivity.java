/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.isorequestlistener.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.annotations.Type;

/**
 *
 * @author Andri D Septian
 */
@Entity(name = "LogActivity")
@Table(name = "log_activity")
public class LogActivity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "id_log_activity")
    @SequenceGenerator(name = "id_log_activity", sequenceName = "id_log_activity")
    @Column(name = "id", nullable = false)
    private Long id;
    
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @Column(name = "request_time", nullable = true)
    private Date requestTime;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @Column(name = "response_time", nullable = true)
    private Date responseTime;

    @Column(name = "request")
    private String request;

    @Column(name = "response")
    @Type(type = "text")
    private String response;

    @Column(name = "request_url")
    @Type(type = "text")
    private String requestUrl;

    @Column(name = "request_body")
    @Type(type = "text")
    private String requestBody;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(Date requestTime) {
        this.requestTime = requestTime;
    }

    public Date getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(Date responseTime) {
        this.responseTime = responseTime;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }

    public String getRequestBody() {
        return requestBody;
    }

    public void setRequestBody(String requestBody) {
        this.requestBody = requestBody;
    }
    
    
    
}
